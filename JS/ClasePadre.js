import {personajes} from "./CreacionDeHeroes.js";

export default class Personaje
{
    constructor(Nombre, Nivel, Vitalidad, Aprendizaje, Resistencia, Vigor, Fuerza, Destreza, Inteligencia, Fe, Suerte)
    {
        this.nombre = Nombre;
        this.nivel = Nivel;
        this.vitalidad = Vitalidad;
        this.aprendizaje = Aprendizaje;
        this.resistencia = Resistencia;
        this.vigor = Vigor;
        this.fuerza = Fuerza;
        this.destreza = Destreza;
        this.inteligencia = Inteligencia;
        this.fe = Fe;
        this.suerte = Suerte;
        this.ps = this.vitalidad * 40;
        this.pc = this.inteligencia * this.aprendizaje;
        this.energia = (this.resistencia + this.destreza)* 2;
        this.pde = this.resistencia * 1.5;
    }

    LanzarPiromancia(nombrePiromancia, coste, enemigo)
    {
        if (coste > pc)
        {
            console.log("No se puede lanzar "+ nombrePiromancia);
        }
        else
        {
            
            enemigo.ps -=(danio- enemigo.resistencia);
            console.log("Ha lanzado " +nombrePiromancia+ " y le has inflinjido " + danio + " puntos de daño");

        }
    }

    LanzarHechizo(nombreHechizo, coste, enemigo)
    {
        if (coste > pc)
        {
            console.log("No se puede lanzar "+ nombreHechizo);
        }
        else
        {
            let danio = coste + inteligencia * 1.5;
            enemigo.ps -=(danio- enemigo.resistencia);
            console.log("Ha lanzado " +nombreHechizo+ " y le has inflinjido " + danio + " puntos de daño");

        }
    }

    Atacar (event)
    {
        let coste = 15;
        let padre = event.target.parentNode;
        let me = padre.querySelector("#nombre").innerText;
        let valor = padre.querySelector("#selType").value;

        let yo = personajes.find(function(element)
        {
            return element.nombre == me;
        });

        console.log(personajes,valor);
        let enemigo = personajes.find(function(element)
        {
            return element.nombre == valor;
        });
        let cuadroEnemigo = document.getElementById(valor);
        let vidaEnemigo = cuadroEnemigo.querySelector("#vida");
        let ataqueEnemigo = cuadroEnemigo.querySelector("#Atacar");
        let hechizoEnemigo = cuadroEnemigo.querySelector("#LanzarHechizo");
        let piromanciaEnemigo = cuadroEnemigo.querySelector("#LanzarPiromancia");

        console.log(ataqueEnemigo);
        if (coste > yo.energia)
        {
            alert("No tienes suficiente energia");
        }
        else
        {
            if(enemigo.ps > 0)
            {
                let danio = coste + yo.fuerza + yo.destreza;
                console.log(enemigo.resistencia);
                enemigo.ps = enemigo.ps - (danio-enemigo.resistencia);
                //console.log(enemigo.resistencia);
                vidaEnemigo.value = enemigo.ps;
                yo.pc -= coste;
                //alert("Has golpeado a tu enemigo y le has inflinjido "+ danio + " puntos de daño");
            }
            else
            {
                ataqueEnemigo.disabled = true;
                hechizoEnemigo.disabled = true;
                piromanciaEnemigo.disabled = true;
                alert("No se puede atacar a ese enemigo, ya esta muerto");
            }
        }
    }
}