import Personaje from "./ClasePadre.js";


const Mercenario = new Personaje("Mercenario", 8, 11, 12, 11, 10, 10, 16, 10, 8, 9);

const Guerrero = new Personaje("Guerrero", 7, 14, 6, 12, 11, 16, 9, 8, 9, 11);

const Heraldo = new Personaje("Heraldo", 9, 12, 10, 9, 12, 12, 11, 8, 13, 11);

const Ladron = new Personaje("Ladron", 5, 10, 11, 10, 9, 9, 13, 10, 9, 14);

const Asesino = new Personaje("Asesino", 10, 10, 14, 11, 10, 10, 14, 11, 9, 10);

const Hechicero = new Personaje("Hechicero", 6, 9, 16, 9, 7, 7, 12, 16, 7, 12);

const Piromantico = new Personaje("Piromantico", 8, 11, 12, 10, 8, 12, 9, 14, 14, 7);

const Clerigo = new Personaje("Clerigo", 7, 10, 14, 9, 7, 12, 8, 7, 16, 13);

const Marginado = new Personaje("Marginado", 1, 10, 10, 10, 10, 10, 10, 10, 10, 10);

export let personajes = [Mercenario, Guerrero, Heraldo, Ladron, Asesino, Hechicero, Piromantico, Clerigo, Marginado];
//console.log(personajes);

//////////////////////////////////////HECHIZOS/////////////////////////////////////////////////////
class Hechizos
{
    constructor(NombreHechizo, Coste)
    {
        this.nombreHechizo = NombreHechizo;
        this.coste = Coste;
    }
}

export let hechizos = [new Hechizos("Flecha de alma", 10), new Hechizos("Gran flecha de alma", 40),new Hechizos("Dardo de farron", 5),new Hechizos("Gran dardo de farron", 20),new Hechizos("Arma magica", 15)];
//console.log(hechizos);

//////////////////////////////////////PIROMACIAS/////////////////////////////////////////////////////
class Piromancias
{
    constructor(NombrePiromacia, Coste)
    {
        this.nombrePiromacia = NombrePiromacia;
        this.coste = Coste;
    }
}

export let piromancias = [new Piromancias("Bola de fuego", 10), new Piromancias("Gran bola de fuego", 40),new Piromancias("Vestigios de lecho del Caos", 5),new Piromancias("Llama profanada", 20),new Piromancias("Neblina tóxica", 15)];
//console.log(piromacias);
