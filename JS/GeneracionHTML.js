import {personajes} from "./CreacionDeHeroes.js";
import {hechizos} from "./CreacionDeHeroes.js";
import {piromancias} from "./CreacionDeHeroes.js";

var contador = 0;
personajes.forEach(function(objeto)
{
    //console.log(objeto.imagen);
    CloneFunction(objeto);
    contador++;
 });
 
 function CloneFunction(objeto) 
 {
    var itm = document.getElementById("item");
    var clon = itm.content.cloneNode(true);
    var padre = clon.querySelector("#padre");
    padre.id = objeto.nombre;

    var vida = document.createElement("progress");
    vida.id = "vida";
    vida.max = objeto.ps;
    vida.value = vida.max;

    padre.appendChild(vida);

    var img = clon.querySelector("img");
    //console.log(img);
    //img.src = objeto.imagen;
 
    var titulo = clon.querySelector("h4");
    titulo.innerText = objeto.nombre;

    const seleccionPersonaje = clon.querySelector("#selType");

    for(let i = 0;i<personajes.length;i++)
    {
        if(personajes[i].nombre == objeto.nombre)
        {
            continue;
        }
        let opcion = document.createElement("option");
        opcion.value = personajes[i].nombre;
        opcion.innerText = personajes[i].nombre;
        //console.log(seleccionPersonaje);
        seleccionPersonaje.appendChild(opcion);
    }
 
    clon.querySelector("h5").innerHTML = objeto.nivel;

    let boton = clon.querySelector("button");
 
    boton.addEventListener("click", objeto.Atacar, false);
    //console.log(boton);

    const seleccionHechizo = clon.querySelector("#selHechizo");

    for(let i = 0;i<hechizos.length;i++)
    {
        let opcion = document.createElement("option");
        opcion.value = hechizos[i].nombreHechizo;
        opcion.innerText = hechizos[i].nombreHechizo;
        seleccionHechizo.appendChild(opcion);
    }

    let botonHechizo = clon.querySelector("#LanzarHechizo");
    botonHechizo.addEventListener("click", objeto.LanzarHechizo,false);

    const seleccionPiro = clon.querySelector("#selPiro");

    for(let i = 0;i<piromancias.length;i++)
    {
        let opcion = document.createElement("option");
        opcion.value = piromancias[i].nombrePiromacia;
        opcion.innerText = piromancias[i].nombrePiromacia;
        seleccionPiro.appendChild(opcion);
    }

    let botonPiro = clon.querySelector("#LanzarPiromancia");
    botonPiro.addEventListener("click", objeto.LanzarPiromancia,false);

    document.querySelector(".colItems").appendChild(clon);
 }
