//////////////////////////////////////PERSONAJES/////////////////////////////////////////////////////

class Personaje
{
    constructor(Nombre, Nivel, Vitalidad, Aprendizaje, Resistencia, Vigor, Fuerza, Destreza, Inteligencia, Fe, Suerte)
    {
        this.nombre = Nombre;
        this.nivel = Nivel;
        this.vitalidad = Vitalidad;
        this.aprendizaje = Aprendizaje;
        this.resistencia = Resistencia;
        this.vigor = Vigor;
        this.fuerza = Fuerza;
        this.destreza = Destreza;
        this.inteligencia = Inteligencia;
        this.fe = Fe;
        this.suerte = Suerte;
        this.ps = this.vitalidad * 40;
        this.pc = this.inteligencia * this.aprendizaje;
        this.energia = (this.resistencia + this.destreza)* 2;
        this.pde = this.resistencia * 1.5;
    }

    LanzarPiromancia(nombrePiromancia, coste, enemigo)
    {
        if (coste > pc)
        {
            console.log("No se puede lanzar "+ nombrePiromancia);
        }
        else
        {
            
            enemigo.ps -=(danio- enemigo.resistencia);
            console.log("Ha lanzado " +nombrePiromancia+ " y le has inflinjido " + danio + " puntos de daño");

        }
    }

    LanzarHechizo(nombreHechizo, coste, enemigo)
    {
        if (coste > pc)
        {
            console.log("No se puede lanzar "+ nombreHechizo);
        }
        else
        {
            let danio = coste + inteligencia * 1.5;
            enemigo.ps -=(danio- enemigo.resistencia);
            console.log("Ha lanzado " +nombreHechizo+ " y le has inflinjido " + danio + " puntos de daño");

        }
    }

    Atacar (event)
    {
        let coste = 15;
        let padre = event.target.parentNode;
        let me = padre.querySelector("#nombre").innerText;
        let valor = padre.querySelector("#selType").value;

        let yo = personajes.find(function(element){
            return element.nombre == me;
        });

        console.log(personajes,valor);
        let enemigo = personajes.find(function(element){
            return element.nombre == valor;
        });
        let cuadroEnemigo = document.getElementById(valor);
        let vidaEnemigo = cuadroEnemigo.querySelector("#vida");
        let ataqueEnemigo = cuadroEnemigo.querySelector("#Atacar");
        let hechizoEnemigo = cuadroEnemigo.querySelector("#LanzarHechizo");
        let piromanciaEnemigo = cuadroEnemigo.querySelector("#LanzarPiromancia");

        console.log(ataqueEnemigo);
        if (coste > yo.energia)
        {
            alert("No tienes suficiente energia");
        }
        else
        {
            if(enemigo.ps > 0)
            {
                let danio = coste + yo.fuerza + yo.destreza;
                console.log(enemigo.resistencia);
                enemigo.ps = enemigo.ps - (danio-enemigo.resistencia);
                //console.log(enemigo.resistencia);
                vidaEnemigo.value = enemigo.ps;
                yo.pc -= coste;
                //alert("Has golpeado a tu enemigo y le has inflinjido "+ danio + " puntos de daño");
            }
            else
            {
                ataqueEnemigo.disabled = true;
                hechizoEnemigo.disabled = true;
                piromanciaEnemigo.disabled = true;
                alert("No se puede atacar a ese enemigo, ya esta muerto");
            }
        }
    }
}

const Mercenario = new Personaje("Mercenario", 8, 11, 12, 11, 10, 10, 16, 10, 8, 9);

const Guerrero = new Personaje("Guerrero", 7, 14, 6, 12, 11, 16, 9, 8, 9, 11);

const Heraldo = new Personaje("Heraldo", 9, 12, 10, 9, 12, 12, 11, 8, 13, 11);

const Ladron = new Personaje("Ladron", 5, 10, 11, 10, 9, 9, 13, 10, 9, 14);

const Asesino = new Personaje("Asesino", 10, 10, 14, 11, 10, 10, 14, 11, 9, 10);

const Hechicero = new Personaje("Hechicero", 6, 9, 16, 9, 7, 7, 12, 16, 7, 12);

const Piromantico = new Personaje("Piromantico", 8, 11, 12, 10, 8, 12, 9, 14, 14, 7);

const Clerigo = new Personaje("Clerigo", 7, 10, 14, 9, 7, 12, 8, 7, 16, 13);

const Marginado = new Personaje("Marginado", 1, 10, 10, 10, 10, 10, 10, 10, 10, 10);

var personajes = [Mercenario, Guerrero, Heraldo, Ladron, Asesino, Hechicero, Piromantico, Clerigo, Marginado];
//console.log(personajes);

//////////////////////////////////////HECHIZOS/////////////////////////////////////////////////////
class Hechizos
{
    constructor(NombreHechizo, Coste)
    {
        this.nombreHechizo = NombreHechizo;
        this.coste = Coste;
    }
}

var hechizos = [new Hechizos("Flecha de alma", 10), new Hechizos("Gran flecha de alma", 40),new Hechizos("Dardo de farron", 5),new Hechizos("Gran dardo de farron", 20),new Hechizos("Arma magica", 15)];
//console.log(hechizos);

//////////////////////////////////////PIROMACIAS/////////////////////////////////////////////////////
class Piromancias
{
    constructor(NombrePiromacia, Coste)
    {
        this.nombrePiromacia = NombrePiromacia;
        this.coste = Coste;
    }
}

var piromacias = [new Piromancias("Bola de fuego", 10), new Piromancias("Gran bola de fuego", 40),new Piromancias("Vestigios de lecho del Caos", 5),new Piromancias("Llama profanada", 20),new Piromancias("Neblina tóxica", 15)];
//console.log(piromacias);

////////////////////////////////////////GENERAR PAGINA/////////////////////////////////////////////
var contador = 0;
personajes.forEach(function(objeto)
{
    //console.log(objeto.imagen);
    CloneFunction(objeto);
    contador++;
 });
 
 function CloneFunction(objeto) 
 {
    var itm = document.getElementById("item");
    var clon = itm.content.cloneNode(true);
    var padre = clon.querySelector("#padre");
    padre.id = objeto.nombre;

    var vida = document.createElement("progress");
    vida.id = "vida";
    vida.max = objeto.ps;
    vida.value = vida.max;

    padre.appendChild(vida);

    var img = clon.querySelector("img");
    //console.log(img);
    //img.src = objeto.imagen;
 
    var title = clon.querySelector("h4");
    title.innerHTML = objeto.nombre;

    const seleccionPersonaje = clon.querySelector("#selType");

    for(let i = 0;i<personajes.length;i++)
    {
        if(personajes[i].nombre == objeto.nombre)
        {
            continue;
        }
        let opcion = document.createElement("option");
        opcion.value = personajes[i].nombre;
        opcion.innerText = personajes[i].nombre;
        //console.log(seleccionPersonaje);
        seleccionPersonaje.appendChild(opcion);
    }
 
    clon.querySelector("h5").innerHTML = objeto.nivel;

    var boton = clon.querySelector("button");
 
    boton.addEventListener("click", objeto.Atacar, false);
    //console.log(boton);

    document.querySelector(".colItems").appendChild(clon);
 }
